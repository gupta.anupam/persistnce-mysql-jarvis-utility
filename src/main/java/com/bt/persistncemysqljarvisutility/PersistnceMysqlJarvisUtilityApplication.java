package com.bt.persistncemysqljarvisutility;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PersistnceMysqlJarvisUtilityApplication {

	public static void main(String[] args) {
		SpringApplication.run(PersistnceMysqlJarvisUtilityApplication.class, args);
	}

}
