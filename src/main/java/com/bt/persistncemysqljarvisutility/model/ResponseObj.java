package com.bt.persistncemysqljarvisutility.model;

public class ResponseObj {

    private String msg;

    @Override
    public String toString() {
        return "ResponseObj{" +
                "msg='" + msg + '\'' +
                '}';
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
