package com.bt.persistncemysqljarvisutility.model;

public class NetworkDetails {


    private String objectType;

    @Override
    public String toString() {
        return "NetworkDetails{" +
                "objectType='" + objectType + '\'' +
                '}';
    }

    public String getObjectType() {
        return objectType;
    }

    public void setObjectType(String objectType) {
        this.objectType = objectType;
    }


}
