package com.bt.persistncemysqljarvisutility.model;

import javax.persistence.Entity;

public class Domain {

    private int domain;
    private boolean sourceRef;
    private  String reportName;
    private MsoDetails msoDetails;
    private Inventory inventory;

    @Override
    public String toString() {
        return "Domain{" +
                "domain=" + domain +
                ", sourceRef=" + sourceRef +
                ", reportName='" + reportName + '\'' +
                ", msoDetails=" + msoDetails +
                ", inventory=" + inventory +
                ", networkDetails=" + networkDetails +
                '}';
    }

    private NetworkDetails networkDetails;

    public int getDomain() {
        return domain;
    }

    public void setDomain(int domain) {
        this.domain = domain;
    }

    public boolean isSourceRef() {
        return sourceRef;
    }

    public void setSourceRef(boolean sourceRef) {
        this.sourceRef = sourceRef;
    }

    public String getReportName() {
        return reportName;
    }

    public void setReportName(String reportName) {
        this.reportName = reportName;
    }

    public MsoDetails getMsoDetails() {
        return msoDetails;
    }

    public void setMsoDetails(MsoDetails msoDetails) {
        this.msoDetails = msoDetails;
    }

    public Inventory getInventory() {
        return inventory;
    }

    public void setInventory(Inventory inventory) {
        this.inventory = inventory;
    }

    public NetworkDetails getNetworkDetails() {
        return networkDetails;
    }

    public void setNetworkDetails(NetworkDetails networkDetails) {
        this.networkDetails = networkDetails;
    }
}
