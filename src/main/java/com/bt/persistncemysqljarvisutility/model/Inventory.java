package com.bt.persistncemysqljarvisutility.model;

public class Inventory {

    private String snaId;

    @Override
    public String toString() {
        return "Inventory{" +
                "snaId='" + snaId + '\'' +
                '}';
    }

    public String getSnaId() {
        return snaId;
    }

    public void setSnaId(String snaId) {
        this.snaId = snaId;
    }

}
